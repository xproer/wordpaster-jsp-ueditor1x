<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ page contentType="text/html;charset=utf-8"%>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<% out.clear();
/*	
	更新记录：
		2013-01-25 取消对SmartUpload的使用，改用commons-fileupload组件。因为测试发现SmartUpload有内存泄露的问题。
*/
//String path = request.getContextPath();
//String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String clientCookie = request.getHeader("Cookie");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>编辑器完整版实例-1.2.6.0</title>
	<script type="text/javascript" src="ueditor.config.js"></script>
	<script type="text/javascript" src="ueditor.all.js" ></script>
	<link type="text/css" rel="Stylesheet" href="demo.css" />
	<link type="text/css" rel="Stylesheet" href="WordPaster/js/jquery-ui-1.8.4/themes/base/jquery-ui.css" />
    <script type="text/javascript" src="WordPaster/js/json2.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="WordPaster/js/jquery-1.4.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="WordPaster/js/jquery-ui-1.8.4/ui/jquery-ui.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="WordPaster/js/w.jqueryui.js" charset="utf-8"></script>
    <script type="text/javascript" src="demo.js" charset="utf-8"></script>
</head>
<body>
	<div id="demos"></div>	
	<textarea name="后台取值的key" id="myEditor">这里写你的初始化内容</textarea>
	<script type="text/javascript">
		var pos = window.location.href.lastIndexOf("/");
        var api = [
            window.location.href.substr(0, pos + 1),
            "upload.jsp"
        ].join("");
		WordPaster.getInstance({
			//上传接口：http://www.ncmem.com/doc/view.aspx?id=d88b60a2b0204af1ba62fa66288203ed
	        PostUrl:api,
			//为图片地址增加域名：http://www.ncmem.com/doc/view.aspx?id=704cd302ebd346b486adf39cf4553936
            ImageUrl: "",
            //设置文件字段名称：http://www.ncmem.com/doc/view.aspx?id=c3ad06c2ae31454cb418ceb2b8da7c45
            FileFieldName: "file",
            //提取图片地址：http://www.ncmem.com/doc/view.aspx?id=07e3f323d22d4571ad213441ab8530d1
            ImageMatch: '',
	        Cookie: '<%=clientCookie%>'	        
	    });//加载控件
		
		 UE.getEditor('myEditor',{onready:function(){//创建一个编辑器实例
			 WordPaster.getInstance().SetEditor(this);
			 
			 //WordPaster快捷键 Ctrl + V
		     this.addshortcutkey({
		         "wordpaster": "ctrl+86"
		     });
		 }});
	</script>
</body>
</html>